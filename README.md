# Recoil

Recoil is a vibrant, dark theme with first-class support for **Rust** projects.

This extension was created to be fully compatible with `VSCodium`.

## Warning

If you enjoy this theme, you are **strongly encouraged** to clone or fork this repository
and maintain your own version. This is completely free and open source software.

You can freeze it as-is or make changes to suit your particular needs.

## Stability

Until v1.0.0, this project will experience breaking changes. These may occur at any time, for any
reason. 

## Contributing

Please visit the project [Gitlab repository][project-repo], open a new ticket that outlines the scope of
your contribution(s) and be ready to discuss your changes. I will respond to you as quickly as possible.
Once your changes are approved, you can open a pull request and I will merge your contribution.

Following the above steps is the best way to ensure your contribution is accepted.

## License

This project is licensed under the [MIT license][license].

### Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted for inclusion in
[`recoil`][project-repo] by you, shall be licensed as MIT, without any additional terms or conditions.

<!-- Links section -->

[project-repo]: https://gitlab.com/ellacrity/recoil-theme
[license]: https://gitlab.com/ellacrity/recoil-theme/-/blob/main/LICENSE